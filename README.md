# JS libs
## Setup
This example is based on the use of Ext-JS.

In your git-project add a new remote:

    git remote add jslibs https://bitbucket.org/detailnet/js-libs.git

Fetch the branches of the repository:

    git fetch jslibs

Specify the location where you would like to add the subtree (*public/libs/extjs*), the remote (*jslibs*) and the branch you want to take the code from (*ext-js/6.0.0*):

    git subtree add --prefix=public/libs/extjs --squash jslibs ext-js/6.0.0

Commit your changes and you are done.